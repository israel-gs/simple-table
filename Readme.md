# Documentación


* Creacion de cuerpo de la tabla.

```javascript
var usersData = [
    {
        name: 'Name1',
        lastname: 'Lastname1',
        username: 'user1'
    },
    {
        name: 'Name2',
        lastname: 'Lastname2',
        username: 'user2'
    },
    {
        name: 'Name3',
        lastname: 'Lastname3',
        username: 'user3'
    }
];

var tableUser = new SimpleTable({
            data: usersData,
            no_data_text: 'No data.',
            columns: [
                {data: 'name', class_name: 'text-center'},
                {data: 'lastname', class_name: 'text-center'},
                {
                    data: function(data) {
                        return `<span>${data.username}</span>`;
                    }, 
                    class_name: 'text-center'
                }
            ]
        });

console.log(tableUser.createBody());
```

``` html
Imprime en consola:

<tr>
   <td class="text-center">Name1</td>
   <td class="text-center">Lastname1</td>
   <td class="text-center"><span>user1</span></td>
</tr>
<tr>
   <td class="text-center">Name2</td>
   <td class="text-center">Lastname2</td>
   <td class="text-center"><span>user2</span></td>
</tr>
<tr>
   <td class="text-center">Name3</td>
   <td class="text-center">Lastname3</td>
   <td class="text-center"><span>user3</span></td>
</tr>


```