let usersData = [
    {
        name: 'Name1',
        lastname: 'Lastname1',
        username: 'user1'
    },
    {
        name: 'Name2',
        lastname: 'Lastname2',
        username: 'user2'
    },
    {
        name: 'Name3',
        lastname: 'Lastname3',
        username: 'user3'
    }
];

let tbl = new SimpleTable({
    data: usersData,
    no_data_text: 'No data.',
    columns: [
        {data: 'name', class_name: 'text-center'},
        {data: 'lastname', class_name: 'text-center'},
        {
            data: function (data) {
                return `<span>${data.username}</span>`;
            },
            class_name: 'text-center'
        }
    ]
});

console.log(tbl.createBody());